// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Init zoom parameters... NOTE(Alex): Should we pack these into the struct?
	ZoomOutDistance = 1200.f;
	ZoomInDistance = 400.f;
	ZoomOutPitch = -80.f;
	ZoomInPitch = -40.f;
	ZoomChange = 0.f;
	ZoomSpeed = 2.f;
	ZoomRate = 0.5f;

	float const ZoomMiddleDistance = (ZoomOutDistance + ZoomInDistance) * 0.5f;
	float const ZoomMiddlePitch = (ZoomOutPitch + ZoomInPitch) * 0.5f;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	
	CameraBoom->TargetArmLength = ZoomDistance = ZoomMiddleDistance;
	CameraBoom->SetRelativeRotation(FRotator(ZoomMiddlePitch, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Input...
	InputSmoothingSpeed = 5.f;
	
	// Movement...
	SpeedValues.Aim = 200.f;
	SpeedValues.Jog = 350.f;
	SpeedValues.Run = 600.f;
	SpeedValues.Walk = 200.f;

	// Gameplay
	WeaponEquipped = EWeaponEquipped::Rifle;
	
	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	//CursorToWorld->SetAbsolute(true, true, true);
	
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// NOTE(Alex): Prioritize <if>, so <if> player press RUN, we won't AIM, and so on
	// NOTE(Alex): Not the most elegant solution (waterfall of <if's>), but it's clean and doing the job
	
	WantedState = ECharacterState::Jogging; // TODO(Alex): Should default state be a parameter?
	
	if (bWalkPressed)
	{
		WantedState = ECharacterState::Walking;
	}
	
	if (bAimPressed)
	{
		WantedState = ECharacterState::Aiming;
	}
	
	if (bRunPressed)
	{
		WantedState = ECharacterState::Running;
	}

	if (CurrentState != WantedState /*&& bCanChangeState*/)
	{
		CurrentState = WantedState;
		UpdateMaxSpeed();
	}

	if (APlayerController* PC = GetController<APlayerController>())
	{
		// NOTE(Alex): Update cursor's world position
		FHitResult TraceHitResult;
		PC->GetHitResultUnderCursor(CURSOR_TRACE_CHANNEL, true, TraceHitResult);

		// NOTE(Alex): Smoothly rotate character to the cursor
		FVector const CursorLocation = TraceHitResult.Location;
		FRotator ToCursor = (CursorLocation - GetActorLocation()).Rotation(); // "Look" at cursor location
		ToCursor = FMath::RInterpTo(GetActorRotation(), ToCursor, DeltaSeconds, 5.f);
		ToCursor.Pitch = ToCursor.Roll = 0.f;

		SetActorRotation(ToCursor);

		/* Update cursor location */
		// NOTE(Alex): Shifted these under SetActorRotation() to avoid cursor world position shifting;
		CursorToWorld->SetWorldLocation(TraceHitResult.ImpactPoint);
		CursorToWorld->SetWorldRotation(TraceHitResult.ImpactNormal.Rotation());

		// NOTE(Alex): Lock input to forward vector <if> we pressed RUN
		if (CurrentState == ECharacterState::Running)
		{
			RawInputVector = GetActorForwardVector();
		}
		
		// NOTE(Alex): Smooth out and consume movement input
		SmoothedInputVector = FMath::VInterpConstantTo(SmoothedInputVector, RawInputVector, DeltaSeconds, InputSmoothingSpeed);
		GetCharacterMovement()->AddInputVector(SmoothedInputVector);

		// NOTE(Alex): Clear raw input for safety;
		RawInputVector = FVector::ZeroVector;
	}
	
	// NOTE(Alex): Zoomed Camera Arm stuff. Maybe will push everything zoom-related to a separate function, later;
	float ZoomInterval = ZoomOutDistance - ZoomInDistance;
	ZoomDistance += ZoomChange * ZoomRate * ZoomInterval;
	ZoomDistance = FMath::Clamp(ZoomDistance, ZoomInDistance, ZoomOutDistance);

	// NOTE(Alex): Calculating ratio between current and max speed;
	float CurrentSpeed = GetCharacterMovement()->Velocity.Size();
	float MaxSpeed = GetCharacterMovement()->MaxWalkSpeed;
	float SpeedAlpha = FMath::Clamp(CurrentSpeed / MaxSpeed, 0.f, 1.f);

	// NOTE(Alex): I like when we zoom slightly more while moving, it generates more oomph;
	float ExtraZoom = FMath::Lerp(0.f, ZoomDistance * 0.1f, SpeedAlpha);
	float FinalZoom = FMath::FInterpTo(CameraBoom->TargetArmLength, ZoomDistance - ExtraZoom, DeltaSeconds, ZoomSpeed);

	CameraBoom->TargetArmLength = FinalZoom;

	// NOTE(Alex): Smoothly rotate camera after setting new distance;
	float ZoomPitch = FMath::GetMappedRangeValueClamped({ ZoomInDistance, ZoomOutDistance }, { ZoomInPitch, ZoomOutPitch }, ZoomDistance);
	FRotator ZoomView = FMath::RInterpTo(CameraBoom->GetRelativeRotation(), { ZoomPitch, 0.f, 0.f }, DeltaSeconds, ZoomSpeed);

	CameraBoom->SetRelativeRotation(ZoomView);
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATDSCharacter::OnMoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATDSCharacter::OnMoveRight);
	PlayerInputComponent->BindAxis("MouseZoom", this, &ATDSCharacter::OnMouseZoom);
	PlayerInputComponent->BindAxis("Aim", this, &ATDSCharacter::OnAim);
	PlayerInputComponent->BindAxis("Run", this, &ATDSCharacter::OnRun);
	PlayerInputComponent->BindAxis("Fire", this, &ATDSCharacter::OnFire);

	PlayerInputComponent->BindAction("AltMove", IE_Pressed, this, &ATDSCharacter::OnAltMove);
	
}

void ATDSCharacter::OnMoveForward(float Value)
{
	RawInputVector.X = Value;
}

void ATDSCharacter::OnMoveRight(float Value)
{
	RawInputVector.Y = Value;
}

void ATDSCharacter::OnMouseZoom(float Value)
{
	ZoomChange = Value;
}

void ATDSCharacter::OnAim(float Value)
{
	bAimPressed = Value != 0.f;
}

void ATDSCharacter::OnRun(float Value)
{
	bRunPressed = Value != 0.f;
}

void ATDSCharacter::OnFire(float Value)
{
	bFirePressed = Value != 0.f;
}

void ATDSCharacter::OnAltMove()
{
	bWalkPressed = !bWalkPressed;
}

void ATDSCharacter::UpdateMaxSpeed()
{
	GetCharacterMovement()->MaxWalkSpeed = TREAT_AS_ARRAY(float, SpeedValues, CurrentState);
}

