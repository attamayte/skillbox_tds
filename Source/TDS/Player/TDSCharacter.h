// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "TDS/TDS.h"
#include "GameFramework/Character.h"
#include "TDSCharacter.generated.h"

UENUM(BlueprintType)
enum class ECharacterState : uint8
{
	Jogging,
	Running,
	Aiming,
	Walking,
};

UENUM(BlueprintType)
enum class EWeaponEquipped : uint8
{
	Unarmed,
    Melee,
	Handgun,
    Rifle,
};

/** Should be union-based array/fields structure */
USTRUCT(BlueprintType)
struct FMaxSpeedValues
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Jog;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Run;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Aim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Walk;
};

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	/** Holds max camera distance */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera)
	float ZoomOutDistance;

	/** Holds min camera distance */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera)
	float ZoomInDistance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera)
	float ZoomOutPitch;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera)
	float ZoomInPitch;
	
	/** Multiplier to camera zoom speed -> [slower < 1.f < faster] */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera)
	float ZoomSpeed;

	/** Multiplier to zoom input (more change for the same input) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera)
	float ZoomRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	FMaxSpeedValues SpeedValues;

protected:
	/** NOTE(Alex): These are potentially virtual, if we happen to inherit from ATDSCharacter, which I'm doubt **/
	/** Input bindings for character **/
	void OnMoveForward(float Value); 
	void OnMoveRight(float Value);
	void OnMouseZoom(float Value);
	void OnAim(float Value);
	void OnRun(float Value);
	void OnFire(float Value);
	void OnAltMove();

	/** Holds fresh input from player */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Input", meta = (AllowPrivateAccess = "true"))
	FVector RawInputVector;

	/** Holds processed input */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Input", meta = (AllowPrivateAccess = "true"))
	FVector SmoothedInputVector;

	/** Holds multiplier to input interpolation speed */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Input", meta = (AllowPrivateAccess = "true"))
	float InputSmoothingSpeed;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay", meta = (AllowPrivateAccess = "true"))
	bool bWalkPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay", meta = (AllowPrivateAccess = "true"))
	bool bAimPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay", meta = (AllowPrivateAccess = "true"))
	bool bRunPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay", meta = (AllowPrivateAccess = "true"))
	bool bFirePressed;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay", meta = (AllowPrivateAccess = "true"))
	EWeaponEquipped WeaponEquipped;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay", meta = (AllowPrivateAccess = "true"))
	ECharacterState CurrentState;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay", meta = (AllowPrivateAccess = "true"))
	ECharacterState WantedState;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay", meta = (AllowPrivateAccess = "true"))
	bool bCanChangeState;
	
	void UpdateMaxSpeed();

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float ZoomDistance;

	/** Holds immediate change in zoom -> [close < 0.f < far] */
	float ZoomChange;
};

