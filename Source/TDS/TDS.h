// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTDS, Log, All);

#define CURSOR_TRACE_CHANNEL ECC_GameTraceChannel1
#define TREAT_AS_ARRAY(_type, _struct, _index) ((_type*)&_struct)[(int)_index]
